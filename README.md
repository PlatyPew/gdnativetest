# GDNativeTest

### Prerequisites
- scons
- clang

### Compilation
To compile, do `scons p=<platform>`

### Platforms available
- windows
- osx
- linux

### Attaching script to node
1. Under the Inspector tab, click on the icon to Create New Resource
2. Add a GDNativeLibrary
3. Attach compiled library to platform you're on. Binary located at demo/bin/<platform>/<library>
    - Windows: `.dll`
    - MacOS: `.dylib`
    - Linux: `.so`
4. Save as `.gdnlib` file.
5. Attach a script to Node and select NativeScript under language. Enter in the Class Name to be used
6. Finally, under Inspector>NativeScript>Library, and `.gdnlib` file.

### Calling gdnative from gdscript
1. Attach script to node, and select nativescript

2. Add method to call in `register_method`
```cpp
// Block.cpp

// Allows method 'test' to be called directly from GDScript
void Block::_register_methods() {
    register_method((char*)"test", &Block::test);
}

Block::Block() {}

Block::~Block() {}

void Block::test() {
    Godot::print("Hello");
}
```

3. Extend script using gdscript 
```gd
const BLOCK = preload("res://scripts/Block.gdns")
var block

func _ready():
    block = BLOCK.new()
    block.test()
```
