#include "block.h"

using namespace godot;

void Block::_register_methods() {
    register_method((char*)"test", &Block::test);
}

Block::Block() {}

Block::~Block() {}

void Block::test() {
    Godot::print("Hello");
}
