#ifndef BLOCK_H
#define BLOCK_H

#include <Godot.hpp>
#include <KinematicBody2D.hpp>
#include <Input.hpp>

namespace godot {

    class Block : public KinematicBody2D {
        GODOT_CLASS(Block, KinematicBody2D)

    public:
        // Important Stuff
        static void _register_methods();

        Block();
        ~Block();

        // Custom stuff
        void test();
    };
}

#endif
