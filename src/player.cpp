#include "player.h"

using namespace godot;

void Player1::_register_methods() {
    register_method((char*)"_process", &Player1::_process);
}

Player1::Player1() {
    motion = Vector2(0, 0);
}

Player1::~Player1() {}

void Player1::_init() {}

void Player1::_process(float delta) {
    UpdateMotionFromInput();
    move_and_slide(motion);
}

void Player1::UpdateMotionFromInput() {
    motion = Vector2(0, 0);

    Input* i = Input::get_singleton();

    if (i->is_action_pressed("ui_up")) {
        motion.y -= SPEED;
    }
    if (i->is_action_pressed("ui_down")) {
        motion.y += SPEED;
    }
    if (i->is_action_pressed("ui_left")) {
        motion.x -= SPEED;
    }
    if (i->is_action_pressed("ui_right")) {
        motion.x += SPEED;
    }
}

void Player2::_register_methods() {
    register_method((char*)"_process", &Player2::_process);
}

Player2::Player2() {
    motion = Vector2(0, 0);
}

Player2::~Player2() {}

void Player2::_init() {}

void Player2::_process(float delta) {
    UpdateMotionFromInput();
    move_and_slide(motion);
}

void Player2::UpdateMotionFromInput() {
    motion = Vector2(0, 0);

    Input* i = Input::get_singleton();

    if (i->is_action_pressed("ui_up")) {
        motion.y += SPEED;
    }
    if (i->is_action_pressed("ui_down")) {
        motion.y -= SPEED;
    }
    if (i->is_action_pressed("ui_left")) {
        motion.x += SPEED;
    }
    if (i->is_action_pressed("ui_right")) {
        motion.x -= SPEED;
    }
}
