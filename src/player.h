#ifndef PLAYER_H
#define PLAYER_H

#include <Godot.hpp>
#include <KinematicBody2D.hpp>
#include <Input.hpp>

namespace godot {

    class Player1 : public KinematicBody2D {
        GODOT_CLASS(Player1, KinematicBody2D)

    private:
        Vector2 motion;

    public:
        // Important Stuff
        static void _register_methods();

        Player1();
        ~Player1();

        void _init();
        void _process(float delta);

        // Custom stuff
        const int SPEED = 100;

        void UpdateMotionFromInput();
    };

    class Player2 : public KinematicBody2D {
        GODOT_CLASS(Player2, KinematicBody2D)

    private:
        Vector2 motion;

    public:
        // Important Stuff
        static void _register_methods();

        Player2();
        ~Player2();

        void _init();
        void _process(float delta);

        // Custom stuff
        const int SPEED = 100;

        void UpdateMotionFromInput();
    };

}

#endif
